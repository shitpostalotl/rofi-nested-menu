menu=$(cat ~/.config/rofi/rofi-nested-menu/menu.json)

if [ -z "$1" ]; then
	nxtmnu=$(echo $menu | jq "keys")
else
	{ # menu
		nxtmnu=$(echo $menu | jq "$1 | keys" 2>/dev/null)
	} || { # item
		$(echo $menu | jq "$1" | sed 's/"//g') &
		killall rofi
	}
fi

echo $nxtmnu | jq '.[]' | sed 's/"//g' | sed "s/^/\n$1./"
